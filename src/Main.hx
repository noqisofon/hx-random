package;

import haxe.unit.TestRunner;
import tests.spaghetti.core.RamdomTest;


class Main {
    static function main() {
        var runner = new TestRunner();

        runner.add( new RamdomTest() );

        runner.run();
    }
}