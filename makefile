.PHONY : run
run: ./export/random-test.n
	neko ./export/random-test.n

./export/random-test.n: ./src/spaghetti/core/Limits.hx ./src/spaghetti/core/Random.hx ./src/Main.hx ./src/tests/spaghetti/core/RamdomTest.hx
	haxe -cp ./src -main Main -neko $@
